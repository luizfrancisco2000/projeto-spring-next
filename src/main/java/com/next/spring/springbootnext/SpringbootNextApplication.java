package com.next.spring.springbootnext;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootNextApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootNextApplication.class, args);
	}

}
